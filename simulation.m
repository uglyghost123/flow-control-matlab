%% 参数设置
NumOfNode = 10;               % 节点总数
NumOfsNode = 2;               % 资源节点数
lambda = 0.2;                   % 请求泊松过程参数
NumOfCon = 10;                  % 资源的数量
linkUsed = cell(NumOfNode);     % 链路使用矩阵
preflow = cell(4,0);            % 初始化流的信息
MTU = 1500;                     % 最小传输单元
gama = 0.001;                    % 迭代步长
simulationTime = 2000;          % 仿真时间
linkHistory = cell(NumOfNode);  % 信息记录

% 得到链路容量
[ inRange ] = create_topo_in_network( NumOfNode, NumOfsNode);

% 初始化链路价格
linkPrice = 1./inRange;
% linkPrice = zeros(NumOfNode);

% duration = zeros(1,NumOfNode);

for time_tt = 1:simulationTime 
    times = 1/random('Poisson',lambda*NumOfNode);
    
    temp_index = randi([ceil((NumOfNode+1)/2),NumOfNode]);
    [ preflow, linkUsed, mindelay ] = routing_flow( NumOfCon, preflow, linkUsed, linkPrice, NumOfNode, NumOfsNode, temp_index );
    
    if(mindelay~=0)        
        teration = floor(times/((MTU*8)/(mindelay*1024^2*100)));
        if(teration>1000)
            teration = 100;
        end
    else
        continue;
    end
    
    for jj = 1:teration
        [ preflow, time ] = flow_running( preflow, MTU );
        %[ linkPrice ] = link_price_update( linkUsed, linkPrice, preflow, gama, inRange );
        [ linkUsed, preflow, linkHistory, linkPrice ] = flow_update( linkUsed, linkPrice, preflow, linkHistory, gama, inRange );
    end
   
end
