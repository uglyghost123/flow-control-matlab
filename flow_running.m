function [ preflow, time ] = flow_running( preflow, MTU )
%FLOW_RUNNING 此处显示有关此函数的摘要
%   此处显示详细说明
%   linkUsed
%   linkPrice
%   preflow
%   MTU

for ii = 1:numel(preflow(1,:))
    if(preflow{3,ii}<(MTU*8/(1024^2)))
        time = preflow{3,ii}/preflow{2,ii};
        preflow(3,ii) = {0};
        % preflow = flow_update(linkUsed,linkPrice, preflow);
    else
        preflow(3,ii) = {preflow{3,ii} - MTU*8/(1024^2)};
        time = (MTU*8/(1024^2))/preflow{2,ii};
    end
end


end

