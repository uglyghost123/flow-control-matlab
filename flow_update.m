function [ linkUsed, preflow, linkHistory, linkPrice ] = flow_update( linkUsed, linkPrice, preflow, linkHistory, gama, inRange )
%FLOW_UPDATE 此处显示有关此函数的摘要
%   此处显示详细说明
%   linkUsed    链路使用信息
%   linkPrice   链路价格信息
%   preflow     需要更新的流

for ii = 1:numel(preflow(1,:))
    
    [ linkPrice ] = link_price_update( linkUsed, linkPrice, preflow(:,ii), gama, inRange );
    flow_route = preflow{1,ii};    % 得到路由信息
    
    if(preflow{3,ii}==0)
        
        for jj = 1:(numel(flow_route)-1)
            candidate = cell(4,0);
            link_temp = linkUsed{flow_route(jj),flow_route(jj+1)};
            for kk = 1:numel(preflow(4,:))
                if(preflow{4,kk}==preflow{4,ii}&&preflow{3,kk}~=0)
                    if(isempty(candidate))
                        candidate = preflow(:,kk);
                    else
                        candidate = [candidate preflow(:,kk)];
                    end
                end
            end
            
            flag = 0;
            temp_bitrate = [];
            for kk = 1:numel(candidate(1,:))
                if(ismember([flow_route(jj),flow_route(jj+1)],candidate{1,kk}))
                    if(candidate{2,kk} > preflow{2,ii})
                        flag = 1;
                    else
                        flag = 2;
                        temp_bitrate = [temp_bitrate,candidate{2,kk}];
                    end
                end
            end
                
            if(flag==2)
                can_bitrate = max(temp_bitrate);
                link_temp(1,link_temp(2,:)==preflow{4,ii}) = can_bitrate;
            else if (flag==0)
                    link_temp(:,link_temp(2,:)==preflow{4,ii}) = [];
                end
            end
            linkUsed(flow_route(jj),flow_route(jj+1)) = {link_temp};
            linkUsed(flow_route(jj+1),flow_route(jj)) = {link_temp};
            linkHistory(flow_route(jj+1),flow_route(jj)) = {[linkHistory{flow_route(jj+1),flow_route(jj)} sum(link_temp(1,:))]};
        end
    else
        price = 0;  % 初始化价格信息
        minDelay = 100;
        for jj = 1:(numel(flow_route)-1)
            tempDelay = 1/inRange(flow_route(jj),flow_route(jj+1));
            if(minDelay>tempDelay)
                minDelay = tempDelay;
            end
        end
        % 得到更新的路由上的price
        for jj = 1:(numel(flow_route)-1)
            price = price + linkPrice(flow_route(jj),flow_route(jj+1));
        end
        
        if(price<minDelay)
            price = minDelay;
        else if (price>1000)
                price = 1000;
            end
        end
        
        bitrate = 1/price;
        preflow(2,ii) = {bitrate};   % 更新流的大小
 
        for jj = 1:(numel(flow_route)-1)
            flag_l = 0;
            temp_bitrate = [];
            link_temp = linkUsed{flow_route(jj),flow_route(jj+1)};
            for kk = 1:numel(preflow(4,:))
                if(preflow{4,kk}==preflow{4,ii}&&kk~=ii)
                    if(ismember([flow_route(jj) flow_route(jj+1)],preflow{1,kk}))
                        if(preflow{2,kk}>bitrate)
                            flag_l = 1;
                            temp_bitrate = [temp_bitrate preflow{2,kk}];
                        end
                    end
                end
            end
            if(flag_l==0)
                link_temp(1,link_temp(2,:)==preflow{4,ii}) = bitrate;
            else
                link_bitrate = max(temp_bitrate);
                link_temp(1,link_temp(2,:)==preflow{4,ii}) = link_bitrate(1);
            end

            linkUsed(flow_route(jj),flow_route(jj+1)) = {link_temp};
            linkUsed(flow_route(jj+1),flow_route(jj)) = {link_temp};
            linkHistory(flow_route(jj+1),flow_route(jj)) = {[linkHistory{flow_route(jj+1),flow_route(jj)} sum(link_temp(1,:))]};
        end
    end
end

end

