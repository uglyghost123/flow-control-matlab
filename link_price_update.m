function [ linkPrice ] = link_price_update( linkUsed, linkPrice, preflow, gama, inRange )
%LINK_PRICE_UPDATE 此处显示有关此函数的摘要
%   此处显示详细说明
%   linkUsed   链路使用情况
%   linkPrice  链路价格
%   preflow    流量情况
%   gama       步长
%   inRange    链路容量信息

for ii = 1:numel(preflow(1,:))
    flow_route = preflow{1,ii};    % 得到路由信息
    
    % 更新路由上的price
    for jj = 1:(numel(flow_route)-1)
%          linkPrice(flow_route(jj),flow_route(jj+1))
%          abs(linkPrice(flow_route(jj),flow_route(jj+1)) + ...
%              gama*(sum(linkUsed{flow_route(jj),flow_route(jj+1)})-inRange(flow_route(jj),flow_route(jj+1))))
        link_used = [];
        link_used = linkUsed{flow_route(jj),flow_route(jj+1)};
        tempPrice = (linkPrice(flow_route(jj),flow_route(jj+1)) + ...
            gama*(sum(link_used(1,:))-inRange(flow_route(jj),flow_route(jj+1))));
        
        if(tempPrice<0)
            tempPrice = 0;
            if(tempPrice>1000)
                tempPrice = 1000;
            end
        end
        linkPrice(flow_route(jj),flow_route(jj+1)) = tempPrice;
        linkPrice(flow_route(jj+1),flow_route(jj)) = tempPrice;
    end
    
end

end

