function [ inRange ] = create_topo_in_network( NumOfNode, NumOfsNode)
%% CREATE_TOPO 此处显示有关此函数的摘要
%   此处显示详细说明
%   NumOfNode    总节点数
%   NumOfsNode   资源节点数

%% 生成网络拓扑
%   [S]    资源节点      x% 不大于50%
%   [R]    中间节点      50%-x%
%   [D]    目的节点      50%
%
%                 |---[D]
%                 |
%       |---[R]---|---[D]
% [S]---|         |
%       |---...---|---...
% ...---|         |
%       |---[R]---|---[D]
%                 |
%                 |---[D]

%% 链路设置
inRange = zeros(NumOfNode);

if(NumOfNode<2*NumOfsNode)
    return;
end

for ii = 1 : floor(NumOfNode/2)
    if(ii <= NumOfsNode)
        for jj = NumOfsNode+1:floor(NumOfNode/2)
            % bandwidth = randi([20,30]);
            % 上层链路带宽设置
            bandwidth = 20;
            inRange(ii,jj) = bandwidth;
            inRange(jj,ii) = bandwidth;
        end
        
    else if(ii<=floor(NumOfNode/2))
            for jj = floor(NumOfNode/2)+1:NumOfNode
                % bandwidth = randi([10,20]);
                % 下层链路带宽设置
                bandwidth = 10;
                inRange(ii,jj) = bandwidth;
                inRange(jj,ii) = bandwidth;
            end
        end
    end
end

end

