function [ routePrice, route_path ] = route_price( sNode, dNode, linkPrice, NumOfNode  )
%   ROUTE PRICE 此处显示有关此函数的摘要
%   sNode        资源节点
%   dNode        请求节点
%   linkPrice    链路价格

% 初始化出发节点
tra_node = dNode;

% 已经选取的节点
selected = dNode;

route_path = cell(1,0);
% 初始化已经选择过的节点
% seled_node = dNode;

% 找到下一跳节点
% nextHop = find(linkPrice(nextHop(ii),:)~=Inf);

% 辅助变量
ii = 1;
flag = 0;
while(numel(selected)~=NumOfNode)
    [ route ] = depth_first_search( tra_node, dNode, linkPrice, sNode, selected );
    if(isempty(route))
        break;
    else
        selected = union(selected,route);
        for jj = 1:ii-1
            if(ismember(route,route_path{jj}))
                flag = 1;
            end
        end
        if(flag)
            break;
        end
        route_path(ii) = {route};
        ii = ii + 1;
    end
end

% 所有链路的价格
routePrice = cell(numel(route_path),1);
for ii = 1:numel(route_path)
    path_temp = route_path{ii};
    for jj = 1:numel(path_temp)-1
        routePrice(ii,1) = {[routePrice{ii,1} linkPrice(path_temp(jj),path_temp(jj+1))]};
    end
end

end

