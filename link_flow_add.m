function [ linkUsed, preflow ] = link_flow_add( linkUsed, route, mindelay, movieMbit, preflow, content )
%LINK_FLOW_ADD 此处显示有关此函数的摘要
%   此处显示详细说明
%   linkPrice   链路价格
%   route       新增内容流路由
%   mindelay    新增内容流比特率
%   movieMbit   视频流大小
%   preflow     之前流信息
%   content     新增内容索引

% 根据延时计算比特率
bitrate = 1/mindelay;

% 记录新增的内容流
flow(1) = {route};            % 路由信息
flow(2) = {bitrate};          % 比特率
flow(3) = {movieMbit};        % 视频块大小
flow(4) = {content};          % 内容块索引
flow = flow';
preflow = [ preflow flow ];  % 更新流   


% 针对路由
% 判断新增流是否对链路产生影响
for ii = 1:(numel(route)-1)
    temp = linkUsed{route(ii),route(ii+1)};
        
    if(isempty(temp)) 
        temp(1,1) = bitrate;
        temp(2,1) = content;
    else if(~ismember(content,temp(2,:)))
            % 如果链路没有该内容流，添加该内容流到链路
            index = numel(temp(1,:))+1;
            temp(1,index) = bitrate;
            temp(2,index) = content;
        else
            % 如果有该内容流，取出内容流大小
            flow_bitrate = temp(1,find(content==temp(2,:)));
            if(bitrate>flow_bitrate)
                % 如果新增内容流更大，更换内容流
                temp(1,find(content==temp(2,:))) = bitrate;
            else
                % 否则不做改变
            end
        end
    end
    % 将更新的内容流记录到linkUsed内
    linkUsed(route(ii),route(ii+1)) = {temp};
    linkUsed(route(ii+1),route(ii)) = {temp};
end

end

