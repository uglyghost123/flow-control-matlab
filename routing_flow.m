function [ preflow, linkUsed, mindelay ] = routing_flow( NumOfCon, preflow, linkUsed, linkPrice, NumOfNode, NumOfsNode, request )
%ROUTING_FLOW 此处显示有关此函数的摘要
%   此处显示详细说明
%   timeNow     仿真时间
%   NumOfCon    资源块数量
%   preFlow     当前网络中存在的流信息
%   linkUsed    链路使用信息
%   linkPric    链路价格
%   NumOfNode   节点总数
%   NumOfsNode  资源节点总数

%% 参数设置
% 每个视频的Mbps
movieMbit = 480;

% 随机生成请求节点
% request = randi([floor(NumOfNode/2)+1,NumOfNode]);
dNode = request;
% 请求的资源块索引
content = randi([1,NumOfCon]);

mindelay = 0;

% 链路价格初始化
% linkPrice = cell(4,0);

%% 找最优路由route
% 资源节点缩索引
sNode = ceil(content/(NumOfCon/NumOfsNode));

%  Input 
%  sNode      资源节点
%  dNode
%  linkPrice
%  NumOfNode
[ price, route ] = route_price(sNode, dNode, linkPrice, NumOfNode);

if (isempty(price))
    return;
end

for ii = 1:numel(price)
    Price(ii) = sum(price{ii});
end

% 找到最小价格
mindelay = min(Price);
minposition = find(Price==mindelay);
        
% 选择一个最小价格的中间节点
% 得到链路路由
route_f = fliplr(route{minposition(1)});

%% 增加一条新流到网络
[ linkUsed, preflow ] = link_flow_add( linkUsed, route_f, mindelay, movieMbit, preflow, content );

end

