function [ route ] = depth_first_search( tra_node, route, linkPrice, sNode, selected )
%DEPTH_FIRST_SEARCH 此处显示有关此函数的摘要
%   此处显示详细说明

% 找到下一跳节点
nextHop = find(linkPrice(tra_node,:)~=Inf);

if(ismember(sNode,nextHop))
    route = [route sNode];
    return;
else
    nextHop = setdiff(nextHop,selected);
    % 选出未选取的节点
    unseled_node = setdiff(nextHop,route);
    if(isempty(unseled_node))
        route = [];
        return;
    end
    tra_node = unseled_node(1);
    route(numel(route)+1) = tra_node;
    % 递归调用
    [ route ] = depth_first_search( tra_node, route, linkPrice, sNode, selected);
end

end

